<?php
/**
 * Strettons functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Strettons
 * @since Strettons 1.0
 */
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function strettons_theme_support() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// Set post thumbnail size.
	set_post_thumbnail_size( 1200, 9999 );

	// Add custom image size used in Cover Template.
	add_image_size( 'strettons-fullscreen', 1980, 9999 );

	// Custom logo.
	$logo_width  = 120;
	$logo_height = 90;

	// If the retina setting is active, double the recommended width and height.
	if ( get_theme_mod( 'retina_logo', false ) ) {
		$logo_width  = floor( $logo_width * 2 );
		$logo_height = floor( $logo_height * 2 );
	}

	add_theme_support(
		'custom-logo',
		array(
			'height'      => $logo_height,
			'width'       => $logo_width,
			'flex-height' => true,
			'flex-width'  => true,
		)
	);
	
}

add_action( 'after_setup_theme', 'strettons_theme_support' );

/**
 * Register and Enqueue Styles.
 */
function strettons_register_styles() {


	wp_enqueue_style( 'style', get_stylesheet_uri() );
	

	//Add CSS.
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.1', 'all');
	wp_enqueue_style( 'font-awsome-min-style', get_template_directory_uri() . '/font-awsome/css/font-awesome.min.css', array(), '1.1', 'all' );
	wp_enqueue_style( 'font-awsome-style', get_template_directory_uri() . '/font-awsome/css/font-awesome.css', array(), '1.1', 'all' );

	wp_enqueue_style( 'style-css', get_template_directory_uri() . '/css/style.css',array(), '1.1', 'all' );
	wp_enqueue_style( 'responsive-style', get_template_directory_uri() . '/css/responsive.css', array(), '1.1', 'all');
	wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/css/slick.css', array(), '1.1', 'all');

}

add_action( 'wp_enqueue_scripts', 'strettons_register_styles' );

/**
 * Register and Enqueue Scripts.
 */
function strettons_register_scripts() {

	$theme_version = wp_get_theme()->get( 'Version' );

	wp_enqueue_script( 'strettons-js', get_template_directory_uri() . '/js/jquery.js', array(), $theme_version, false );
	wp_script_add_data( 'strettons-js', 'async', true );

	wp_enqueue_script( 'strettons-proper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js', array(), $theme_version, false );
	wp_enqueue_script( 'strettons-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), $theme_version, false );

	wp_enqueue_script( 'strettons-slick', 'https://cdn.jsdelivr.net/jquery.slick/1.3.15/slick.min.js', array(), $theme_version, false );
	wp_enqueue_script( 'strettons-custom', get_template_directory_uri() . '/js/custom.js', array(), $theme_version, false );
	wp_enqueue_script( 'strettons-script', get_template_directory_uri() . '/js/script.js', array(), $theme_version, false );
}

add_action( 'wp_enqueue_scripts', 'strettons_register_scripts');

// Custom Scripting to Move JavaScript from the Head to the Footer
function remove_head_scripts() {
remove_action('wp_head', 'wp_print_scripts');
remove_action('wp_head', 'wp_print_head_scripts', 9);
remove_action('wp_head', 'wp_enqueue_scripts', 1);

add_action('wp_footer', 'wp_print_scripts', 5);
add_action('wp_footer', 'wp_enqueue_scripts', 5);
add_action('wp_footer', 'wp_print_head_scripts', 5);
}
add_action( 'wp_enqueue_scripts', 'remove_head_scripts' );

function filter_plugin_updates( $value ) {
    unset( $value->response['advanced-custom-fields-pro-master/acf.php'] );
    return $value;
}
add_filter( 'site_transient_update_plugins', 'filter_plugin_updates' );

/**
 * Get the information about the logo.
 *
 * @param string $html The HTML output from get_custom_logo (core function).
 * @return string
 */
function strettons_get_custom_logo( $html ) {

	$logo_id = get_theme_mod( 'custom_logo' );

	if ( ! $logo_id ) {
		return $html;
	}

	$logo = wp_get_attachment_image_src( $logo_id, 'full' );

	if ( $logo ) {
		// For clarity.
		$logo_width  = esc_attr( $logo[1] );
		$logo_height = esc_attr( $logo[2] );

		// If the retina logo setting is active, reduce the width/height by half.
		if ( get_theme_mod( 'retina_logo', false ) ) {
			$logo_width  = floor( $logo_width / 2 );
			$logo_height = floor( $logo_height / 2 );

			$search = array(
				'/width=\"\d+\"/iU',
				'/height=\"\d+\"/iU',
			);

			$replace = array(
				"width=\"{$logo_width}\"",
				"height=\"{$logo_height}\"",
			);

			// Add a style attribute with the height, or append the height to the style attribute if the style attribute already exists.
			if ( strpos( $html, ' style=' ) === false ) {
				$search[]  = '/(src=)/';
				$replace[] = "style=\"height: {$logo_height}px;\" src=";
			} else {
				$search[]  = '/(style="[^"]*)/';
				$replace[] = "$1 height: {$logo_height}px;";
			}

			$html = preg_replace( $search, $replace, $html );

		}
	}

	return $html;

}

add_filter( 'get_custom_logo', 'strettons_get_custom_logo' );

// Register widget 

function strettons_sidebar_registration() {

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
		'after_title'   => '</h2>',
		'before_widget' => '',
		'after_widget'  => '',
		'class'         => '',

	);

	// Footer #1.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Footer #1', 'strettons' ),
				'id'          => 'sidebar-1',
				'description' => __( 'Widgets in this area will be displayed in the first column in the footer.', 'strettons' ),
			)
		)
	);

	// Footer #2.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Footer #2', 'strettons' ),
				'id'          => 'sidebar-2',
				'description' => __( 'Widgets in this area will be displayed in the second column in the footer.', 'strettons' ),
			)
		)
	);

}

add_action( 'widgets_init', 'strettons_sidebar_registration' );

/*Register Menu Location*/

if ( ! function_exists( 'strettons_register_nav_menu' ) ) {
 
    function strettons_register_nav_menu(){
        register_nav_menus( array(
            'primary_menu' => __( 'Primary Menu', 'strettons' ),
            'footer_menu'  => __( 'Footer Menu', 'strettons' ),
        ) );
    }
    add_action( 'after_setup_theme', 'strettons_register_nav_menu', 0 );
}

/**
 * Register a post type 'industry'.
 *
 * @see register_post_type for registering post types.
 */
function industries_post_init() {
    $labels = array(
        'name'                  => _x( 'Industries', 'Post type general name', 'industry' ),
        'singular_name'         => _x( 'Industry', 'Post type singular name', 'industry' ),
        'menu_name'             => _x( 'Industries', 'Admin Menu text', 'industry' ),
        'name_admin_bar'        => _x( 'Industry', 'Add New on Toolbar', 'industry' ),
        'add_new'               => __( 'Add New', 'industry' ),
        'add_new_item'          => __( 'Add New industry', 'industry' ),
        'new_item'              => __( 'New industry', 'industry' ),
        'edit_item'             => __( 'Edit industry', 'industry' ),
        'view_item'             => __( 'View industry', 'industry' ),
        'all_items'             => __( 'All Industries', 'industry' ),
        'search_items'          => __( 'Search Industries', 'industry' ),
        'parent_item_colon'     => __( 'Parent Industries:', 'industry' ),
        'not_found'             => __( 'No Industries found.', 'industry' ),
        'not_found_in_trash'    => __( 'No Industries found in Trash.', 'industry' ),
        'featured_image'        => _x( 'Industry Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'industry' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'industry' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'industry' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'industry' ),
        'archives'              => _x( 'Industry archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'industry' ),
        'insert_into_item'      => _x( 'Insert into industry', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'industry' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this industry', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'industry' ),
        'filter_items_list'     => _x( 'Filter Industries list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'industry' ),
        'items_list_navigation' => _x( 'Industries list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'industry' ),
        'items_list'            => _x( 'Industries list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'industry' ),
    );     
    $args = array(
        'labels'             => $labels,
        'description'        => 'Industry custom post type.',
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'industry' ),
        'menu_icon'          => 'dashicons-networking',
        'capability_type'    => 'post',
        'menu_position'      => 20,
        'supports'           => array( 'title', 'editor', 'author', 'thumbnail' ),
        'show_in_rest'       => true
    );
      
    register_post_type( 'industry', $args );
}
add_action( 'init', 'industries_post_init' );

/**
 * Register a 'type' taxonomy for post type 'industry', with a rewrite to match industry CPT slug.
 *
 * @see register_post_type for registering post types.
 */
function create_industry_tax_rewrite() {
    register_taxonomy( 'type', 'industry', array(
        'rewrite'      => array( 'slug' => 'industries/type' ),
        'hierarchical' => true

    ) );
}
add_action( 'init', 'create_industry_tax_rewrite', 0 );

// Options page settings
add_action('acf/init', 'my_acf_op_init');
function my_acf_op_init() {

    // Check function exists.
    if( function_exists('acf_add_options_page') ) {

        // Add parent.
        $parent = acf_add_options_page(array(
            'page_title'  => __('Theme General Settings'),
            'menu_title'  => __('Theme Settings'),
            'redirect'    => false,
        ));
    }
}

function add_additional_class_on_li($classes, $item, $args) {
    if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
    }
    return $classes;
}
add_filter('nav_menu_css_class', 'add_additional_class_on_li', 1, 3);

add_filter( 'nav_menu_link_attributes', 'wpse156165_menu_add_class', 10, 3 );

function wpse156165_menu_add_class( $atts, $item, $args ) {
    $class = 'nav-link'; // or something based on $item
    if(isset($args->add_li_class)) {

    $atts['class'] = $class;

	}
    return $atts;
}