 <footer class="footer-main">
         <div class="container-fluid">
            <div class="container-box-margin">
               <div class="row">
                  <div class="col-md-5">
                     <div class="book-review-box">
                     <?php echo get_field('footer_left_section','options'); ?>   
                    </div>
                  </div>
                  <div class="col-md-7">
                     <div class="map-responsive">
                     <?php echo get_field('footer_right_secction','options'); ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </footer>
      <!-- Subfooter -->
      <footer class="subfooter">
         <div class="container-fluid">
            <div class="container-box-margin">
               <div class="row">
                  <div class="col-xl-7 col-lg-7 col-sm-12 text-center text-lg-left">
                     <?php if(get_field('footer_logo','options')) : ?>
                     <div class="footer-logo-icon">
                           <?php foreach(get_field('footer_logo','options') as $footlogo) : ?>
                            <a href="<?php echo $footlogo['link']; ?>"><img src="<?php echo $footlogo['logo']; ?>" alt="logo" class="img-fluid"></a>
                           <?php endforeach; ?>
                      </div>
                   <?php endif; ?>
                  </div>
              
               <div class="col-xl-5 col-lg-5 col-sm-12 text-right">

               <div class="footer-menu">
                <p>&#169; <?php echo date('Y'); ?> <?php echo get_field('privacy_policy','options') ?></p>
               </div>
               <div class="footer-menu">
               <?php wp_nav_menu(array('container' => false, 'theme_location'=>'footer_menu','menu_class'=>'')); ?>
               </div>
               </div>
                  
               </div>
            </div>
         </div>
      </footer>
      <?php wp_footer(); ?>
   </body>
</html>