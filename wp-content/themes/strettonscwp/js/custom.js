$('.slick-codepen').slick({
  draggable: true,
  accessibility: false,
  centerMode: true,
  variableWidth: true,
  slidesToShow: 1,
  arrows: false,
   rtl: false,
  dots: true,
autoplay: true,

    autoplaySpeed: 5000,
  swipeToSlide: true,
  infinite: false
});
