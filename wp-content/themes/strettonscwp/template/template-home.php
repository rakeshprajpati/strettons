<?php /*Template Name: Home Page*/ ?>
<?php get_header(); ?>
<!-- Banner -->
<section class="banner-two bg-banner-two">
         <div class="container-fluid">
         <?php $banner_section = get_field('banner_section'); ?>
            <div class="container-box-margin">
               <div class="row">
                  <div class="col-md-7 col-sm-12 d-flex flex-column align-items-stretch justify-content-center">
                     <!-- Content Block -->
                     <div class="block bottom-space">
                        <h1><?php echo $banner_section['heading']; ?></h1>
                        <h6><?php echo $banner_section['sub_heading']; ?></h6>
                        <!-- Action Button -->
                        <a href="<?php echo $banner_section['button_link']; ?>" class="btn btn-main-md"><?php echo $banner_section['button_text']; ?></a>
                     </div>
                  </div>
              <div class="col-md-5 col-sm-12 d-flex flex-column align-items-stretch justify-content-center">
                   <div>
                    <img class="img-fluid" src="<?php echo $banner_section['background_image']; ?>" />
                  </div>
              </div> 
               </div>
            <div class="row">
                  <div class="col-md-12 col-sm-12">
                     <div class="wrapper-slider">
                        <?php if(count($banner_section['banner_image'])> 0) : ?>
                        <div class="slick-codepen">
                           <?php foreach($banner_section['banner_image'] as $slider) : ?>
                           <div>
                           <img src="<?php echo $slider['slider_images']; ?>" alt="slider" />
                           </div>
                        <?php endforeach; ?>
                        </div>
                     <?php endif; ?>
                     </div>     
                  </div>
            </div>
            </div>
         </div>
      </section>
      <!--====  End of Banner  ====-->
	  
      <!--===========================
         =            Local Knowledge            =
         ============================-->
      <section class="section about">
         <div class="container-fluid ">
            <div class="container-box-margin">
               <?php $expert_section = get_field('expert_section'); ?>
               <div class="row">
                  <div class="col-lg-6 col-md-6 align-self-center ml-lg-auto">
                     <div class="content-block">
                        <h2><?php echo $expert_section['heading']; ?></h2>
                        <h4><?php echo $expert_section['sub_heading']; ?></h4>
                        <div class="year-exp-box year-exp-box d-flex justify-content-left">
                           <div class="year-exp">
                              <?php echo $expert_section['block_first']; ?>
                           </div>
                           <div class="high-skill-people-box">
                              <?php echo $expert_section['block_second']; ?>
                           </div>
                        </div>
                        <div class="description-one">
                              <?php echo $expert_section['description']; ?>
                        </div>
                        <ul class="list-inline">
                           <li class="list-inline-item">
                              <a href="<?php echo $expert_section['button_link']; ?>" class="btn btn-main-md"><?php echo $expert_section['button_name']; ?></a>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-6 col-md-6 align-self-center">
                     <div class="image-block two bg-about">
                        <img class="img-fluid" src="<?php echo $expert_section['image']; ?>" alt="bg">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!--====  End of Local Knowledge  ====-->
	  
      <!--==============================
         =            Start Industries    =
         ===============================-->
      <?php $industries_section = get_field('industries_section'); ?>
      <section class="section speakers bg-speaker overlay-lighter" style="background-image: url('<?php echo $industries_section['background_image']; ?>');">
         <div class="container-fluid">
            <div class="container-box-margin">

               <div class="row">
                  <div class="col-12">
                     <!-- Section Title -->
                     <div class="section-title white">
                        <h3><?php echo $industries_section['heading']; ?></h3>
                        <p><?php echo $industries_section['sub_heading']; ?></p>
                     </div>
                  </div>
               </div>
                     <?php 
                      $args = array(  
                            'post_type' => 'industry',
                            'post_status' => 'publish',
                            'posts_per_page' => 5, 
                             'orderby' => 'date', 
                             'order' => 'ASC', 
                        );
                     ?>
               <div class="row d-flex justify-content-center">
                  <?php $loop = new WP_Query( $args ); 
                        while ( $loop->have_posts() ) : $loop->the_post(); ?>
                  <div class="box">
                     <!-- Industries 1 -->
                     <div class="speaker-item">
                        <div class="image">
                           <?php echo get_the_post_thumbnail(get_the_ID(), 'medium'); ?>
                           <div class="primary-overlay"></div>
                           <div class="socials">
                              <a href="<?php echo get_field('hover_link', get_the_ID()); ?>" class="btn find-out-more-buttn">
                                 <?php if(get_field('hover_text',get_the_ID())){ echo get_field('hover_text', get_the_ID()); }else{ echo 'Find Out More'; } ?>
                              </a>
                           </div>
                        </div>
                        <div class="content text-left <?php echo get_field('color_class', get_the_ID()); ?>">
                           <h5><a href="<?php echo get_field('hover_link', get_the_ID()); ?>"><?php the_title(); ?></a></h5>
                           <?php //the_permalink(); ?>
                        </div>
                     </div>
                  </div>

                   <?php  endwhile;
                        wp_reset_postdata(); ?>
               </div>
            </div>
         </div>
      </section>
      <!--====  End of Industries  ====-->

<?php get_footer(); ?>