<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- SITE TITTLE -->
      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>
      
      <!--CSS STYLE -->
      <?php wp_head(); ?>
   </head>
   <body class="body-wrapper" <?php body_class(); ?>>
      <section class="top-colored-line"></section>
      <!--========================================
         =            Navigation Section            =
         =========================================-->
      <nav class="navbar main-nav border-less fixed-top navbar-expand-lg p-0">
         <div class="container-fluid p-0">
            <!-- logo -->
            <a class="navbar-brand" href="<?php site_url(); ?>">
            <img src="<?php echo get_field('logo','options');?>" alt="logo" class="img-fluid"> 
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
            <?php wp_nav_menu(array('container' => false, 'theme_location'=>'primary_menu', 'menu_class'=>'navbar-nav mx-auto','add_li_class'  => 'nav-item')); ?>
               <?php if(get_field('menu_button_text','options')){ ?>
               <a href="<?php echo get_field('menu_button_link','options'); ?>" class="ticket">
               <span><?php echo get_field('menu_button_text','options'); ?></span>
               </a>
            <?php } ?>
            </div>
         </div>
      </nav>