<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- SITE TITTLE -->
      <meta charset="<?php bloginfo( 'charset' ); ?>">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title><?php wp_title(''); ?> | Strettons</title>
      
      <!--CSS STYLE -->
      <?php wp_head(); ?>
   </head>
   <body class="body-wrapper" <?php body_class(); ?>>
      <section class="top-colored-line"></section>
      <!--========================================
         =            Navigation Section            =
         =========================================-->
      <nav class="navbar main-nav border-less fixed-top navbar-expand-lg p-0">
         <div class="container-fluid p-0">
            <!-- logo -->
            <a class="navbar-brand" href="index.html">
            <img src="<?php echo get_template_directory_uri(); ?>/images/logo.jpg" alt="logo" class="img-fluid"> 
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="fa fa-bars"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
               <ul class="navbar-nav mx-auto">
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:void(0);">About Us</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:void(0);">Our People</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:void(0);">Services</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:void(0);">Industries</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:void(0);">News</a>
                  </li>
                  <li class="nav-item">
                     <a class="nav-link" href="javascript:void(0);">Contact</a>
                  </li>
               </ul>
               <a href="javascript:void(0);" class="ticket">
               <span>Book Your Free Review Now</span>
               </a>
            </div>
         </div>
      </nav>